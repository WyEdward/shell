# shell统一问题解决

### $'\r': 未找到命令

```shell
# 考虑到代码是从windows复制过来的，脚本可能在格式上存在问题

# ubuntu 解决方案：
sudo apt-get install dos2unix
dos2unix **.sh

# centos 解决方案：
yum install dos2unix
dos2unix **.sh
```

### Syntax error: end of file unexpected (expecting "fi")

解决：

```shell
vim run_app.sh
```

输入 `:set ff`  ->  回车即可看到当前脚本格式`dos`
修改 `:set ff=unix`
再输入":set ff"来查看格式,可以看到当前脚本格式变成了我们想要的"unix"了
最后 :wq! 保存退出即可
