echo "\n================= ↓↓↓↓↓↓ for循环 ↓↓↓↓↓↓ ================="

for receive_num in 1 2 3 4 5
do
    echo "value: $receive_num"
done


app_name_list='demo,test,tool'
# 拆分以英文逗号分隔的字符串，循环打印
for app_name in ${app_name_list//,/ }; do
    echo "app_name: [${app_name}]"
done



for receive_num in 1 2 3 4 5; do echo "* value: $receive_num"; done;

echo "\n================= ↓↓↓↓↓↓ while循环 ↓↓↓↓↓↓ ================="

int=1
while(( $int<=3 ))
do
    echo $int
    let "int++"
done


echo "\n================= ↓↓↓↓↓↓ 无限循环 ↓↓↓↓↓↓ ================="

#   # 示例1
#   for (( ; ; ))
#
#   # 示例2
#   while :
#   do
#       command
#   done
#
#   # 示例3
#   while true
#   do
#       command
#   done


echo "\n================= ↓↓↓↓↓↓ until循环 ↓↓↓↓↓↓ ================="

a=0
until [ ! $a -lt 3 ]
do
   echo $a
   a=`expr $a + 1`
done


echo "\n================= ↓↓↓↓↓↓ 跳出循环-break ↓↓↓↓↓↓ ================="
# break:跳出最外层循环
while :
do
    echo -n "输入1到5之间的数字:"
    read receive_num
    case $receive_num in
        1|2|3|4|5) echo "你输入的数字为 $receive_num!"
        ;;
        *) echo "你输入的数字不是1到5之间的! 游戏结束"
            break
        ;;
    esac
done


echo "\n================= ↓↓↓↓↓↓ 跳出循环-continue ↓↓↓↓↓↓ ================="
# continue:结束当前循环
while :
do
    echo -n "输入1到5之间的数字: "
    read receive_num
    case $receive_num in
        1|2|3|4|5) echo "你输入的数字为 $receive_num!"
        ;;
        *) echo "你输入的数字不是1到5之间的!"
            continue
            echo "游戏结束"
        ;;
    esac
done
