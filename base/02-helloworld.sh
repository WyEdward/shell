#!/bin/bash

####################################
# @description 第一个shell脚本
#              #!   => 是一个约定的标记，它告诉系统这个脚本需要什么解释器来执行，即使用哪一种 Shell。
#              echo => 命令用于向窗口输出文本。
# @example => sh 02-helloworld.sh
# @author zhengqingya
# @date 2021/10/13 11:31 下午
####################################

echo "Hello World !"
