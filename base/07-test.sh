#!/bin/bash

echo "执行的文件名：${0}"

echo "第1个参数为：${1}"
echo "第2个参数为：${2}"
echo "第3个参数为：${3}"

echo "参数个数：${#}"
echo "所有参数信息(输出作为一个参数，即循环只打印1个参数值)：${*}"
for i in "${*}"; do
    echo ${i}
done

echo "所有参数信息(输出作为传递的n个参数，即可循环打印多个参数值)：${@}"
for i in "${@}"; do
    echo ${i}
done

echo "脚本运行的当前进程ID号：${$}"
echo "后台运行的最后一个进程的ID号：${!}"
echo "显示Shell使用的当前选项(与Linux-set命令功能相同)：${-}"
echo "显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误：${?}"
