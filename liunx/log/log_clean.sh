#!/bin/bash

####################################
# @description 清理日志
# @params $? => 代表上一个命令执行后的退出状态: 0->成功,1->失败
# @example => sh log_clean.sh .
# @author zhengqingya
# @date 2021/9/29 17:30
####################################

# 在执行过程中若遇到使用了未定义的变量或命令返回值为非零，将直接报错退出
set -eu

# 检查参数
if [ "${#}" -lt 1 ]; then
	echo "\033[41;37m 脚本使用示例: sh log_clean.sh /home/logs  \033[0m"
	exit
fi

# 获取脚本第一个参数
LOG_PATH=${1}

echo "================= ↓↓↓↓↓↓ 清理日志: ${LOG_PATH} ↓↓↓↓↓↓ ================="

logs=$(find ${LOG_PATH} -name '*.log')

for log in $logs
do
    echo "clean log: $log"
    cat /dev/null > $log
done

echo "====================================================================="


