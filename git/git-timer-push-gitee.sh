#!/bin/bash

####################################
# @description git-定时提交到gitee
# @example => sh master-merge-dev.sh
# @author zhengqingya
# @date 2021/7/27 9:38 下午
####################################

git_address='https://gitee.com/zhengqingya/test.git'
git_project_name='test'
git_project_file_name='README.md'
# 获取系统当前时间
system_time=$(date "+%Y-%m-%d %H:%M:%S")
msg="【update】提交测试  $system_time"
path='/zhengqingya/soft/timer-task/git-timer-push-gitee'
git_user_name='zhengqingya'
git_user_email='960869719@qq.com'


echo '************************************************ START ************************************************'

echo "------ 当前操作路径：$path ------"

# 进入指定路径
cd $path || exit

# 设置用户名和邮件
git config --global user.name "$git_user_name"
git config --global user.email "$git_user_email"
# 设置全局push的默认分支  -> 【解决：git config --global push.default matching】
git config --global push.default current


# 判断文件夹是否存在 -d
if [[ ! -d "$git_project_name" ]]; then
 echo "------ $git_project_name 项目不存在，执行拉取代码 ------"
 git clone $git_address
else
 echo "------ $git_project_name 项目存在 ------"
fi

# 进入项目
cd $git_project_name || exit

# 判断文件是否存在
if [[ ! -f "$git_project_file_name" ]]; then
 echo "------ README.md文件不存在,执行创建文件指令 ------"
 touch $git_project_file_name
else
 echo "------ README.md文件存在 ------"
fi

# 拉取
git pull

# 追加文件内容
echo " |  [ $system_time ]  | " >> $git_project_file_name

echo "------ 提交时间: ${system_time} ------"

git add .
git commit -m "$msg"
git push


echo '************************************************ END ************************************************'
