#!/bin/bash

####################################
# @description git仓库迁移(所有分支)
# @params $? => 代表上一个命令执行后的退出状态: 0->成功,1->失败
#         ${1} => 脚本第1个参数：旧仓库地址.git
#         ${2} => 脚本第2个参数：新仓库地址.git
# @example => sh git-move.sh https://gitee.com/zhengqingya/docker-compose.git https://github.com/zhengqingya/docker-compose.git
# @author zhengqingya
# @date 2022/6/15 16:01
####################################

# 在执行过程中若遇到使用了未定义的变量或命令返回值为非零，将直接报错退出
set -eu

# 检查参数个数
if [ "${#}" -lt 2 ]; then
	echo "\033[41;37m 脚本使用示例: sh git-move.sh https://gitee.com/zhengqingya/docker-compose.git https://github.com/zhengqingya/docker-compose.git  \033[0m"
	exit
fi

echo "****** 拉取旧仓库地址:   ${1}"
git clone --bare ${1}

echo "****** 进入仓库名称：    ${1##*/}"
cd ${1##*/}

echo "****** 迁移到新仓库地址：${2}"
git push --mirror ${2}
